#!/bin/bash

for i in "$@"
do
case $i in
    -c=*|--config=*)
    export KUBECONFIG=${i#*=}
    ;;
    -n=*|--namespace=*)
    NAMSESPACE="${i#*=}"
    ;;
    *)
        echo "Error arg"
    ;;
esac
done

helm_releases=`helm ls --short --selector autodelete==true --namespace ${NAMSESPACE:-ensi-stage-1}`
CURRENT_TIME_SECONDS=`TZ=UTC date '+%s'`
for RELEASE in ${helm_releases};
  do
  LAST_DEPLOYED=`helm status $RELEASE --namespace ${NAMSESPACE:-ensi-stage-1} --output=json | jq -r '.info.last_deployed'`
  LAST_DEPLOYED_SECONDS=`date -d ${LAST_DEPLOYED} "+%s"`
  SEC_DIFF=`expr $CURRENT_TIME_SECONDS - $LAST_DEPLOYED_SECONDS`
  DAY_DIFF=`expr $SEC_DIFF / 86400` #86400-1 day
  if [ "$DAY_DIFF" -gt 14 ]; then
    echo "$RELEASE is older than a 2 weeks. Proceeding to delete it."
    helm delete $RELEASE --namespace ${NAMSESPACE:-ensi-stage-1}
  fi
done